# Ein minimalistischer Audio-Player
Dieses Repository enthält Code für die Umsetzung eines einfachen Audio-Players mit mehreren Soundfiles. Durch Klick auf die Listeneinträge unter dem Player wird die abzuspielende Datei ausgewählt.

## Demo
Der in diesem Repository enthaltene Code kann unter [https://benjaminbecker.gitlab.io/minimal-audio-player/](https://benjaminbecker.gitlab.io/minimal-audio-player/) aufgerufen werden. 

## Einbindung in html-Seiten
Die Seite muss für die Einbindung des Audio-Players Code in folgender Form enthalten:
```html
<div class="audio-player-container">
    <p id="current-track"></p>
    <audio id="audio-player" controls="controls" src="https://benjaminbecker.gitlab.io/minimal-audio-player/audio/bell.mp3">
        Your browser does not support the <code>audio</code> element.
    </audio>
    <ol id=audio-tracks>
        <li file="https://benjaminbecker.gitlab.io/minimal-audio-player/audio/bell.mp3">Klingel</li>
        <li file="https://benjaminbecker.gitlab.io/minimal-audio-player/audio/labrador.mp3">Labrador</li>
        <li file="https://benjaminbecker.gitlab.io/minimal-audio-player/audio/sos.mp3">SOS</li>
    </ol>
</div>
```
Die ids `current-track`, `audio-player` und `audio-tracks` müssen zwingend vorhanden sein. Die einzelnen Audio-Dateien, stehen jeweils in einem List Item (`<li>`) mit der Eigenschaft `file`.

Irgendwo unterhalb des Audio-Players muss das Skript `index.js` eingebunden werden:
```html
<script src="index.js"></script> 
```

## Bekannte Fehler
+ ~~Die zu Beginn ausgewählte Datei wird nicht angezeigt.~~