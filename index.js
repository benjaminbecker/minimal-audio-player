function setCurrentTrackText(trackName) {
    document.getElementById("current-track").innerHTML = "Es läuft " + trackName;
}

function setCurrentTrack(fileName) {
    var audioPlayer = document.getElementById("audio-player");
    audioPlayer.pause();
    audioPlayer.setAttribute("src", fileName);
}

function play() {
    var audioPlayer = document.getElementById("audio-player");
    audioPlayer.play();
}

function callbackClickTrack(ev) {
    var fileName = ev.target.getAttribute("file");
    var trackName = ev.target.innerHTML;
    setCurrentTrackText(trackName);
    setCurrentTrack(fileName);
    play();
}


var trackNodes = document.getElementById("audio-tracks").children;

for (track of trackNodes) {
    track.addEventListener("click", callbackClickTrack);
}

setCurrentTrackText(trackNodes[0].innerHTML);
setCurrentTrack(trackNodes[0].getAttribute("file"));